/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tempconverter;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 *
 * @author mark
 */
public class TempConverterTest {

    @Test
    void convertFahrenheitToCelsiusTest(){

        assertEquals(0.0, TempConverter.convertFahrenheitToCelsius(32.0), 0.001);
        assertEquals(100.0, TempConverter.convertFahrenheitToCelsius(212.0), 0.001);
        assertEquals(1.0, TempConverter.convertFahrenheitToCelsius(33.8), 0.001);
    }

    @Test
    void convertCelsiusToFahrenheitTest(){

        assertEquals(32.0, TempConverter.convertCelsiusToFahrenheit(0.0), 0.001);
        assertEquals(212.0, TempConverter.convertCelsiusToFahrenheit(100.0), 0.001);
        assertEquals(33.8, TempConverter.convertCelsiusToFahrenheit(1.0), 0.001);
    }

}
