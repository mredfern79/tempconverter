/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tempconverter;

/**
 *
 * @author mark
 */
public class TempConverter {

    private static double celsius;
    private static double fahrenheit;
    private static String conversionType;
    
    /**
     * @param args the command line arguments
     */

    public static void main(String[] args) {
        celsius = 0;
        fahrenheit = 0;
        conversionType = "";

        // Check to see if any arguments have been passed
        switch (args.length) {
            case 0:
                System.out.println("Case 0: Error, no arguments provided. Please provide either 1 or 3 " +
                        "additional arguments.");
                converterInstructions();
                break;
            case 1:
                System.out.println("Case 1: Error, please provide 'c2f' or 'f2c' as  1st argument and either 1 or 3 " +
                        "additional arguments");
                converterInstructions();
                break;
            case 2:
                checkCorrectInputForConversionType(args[0]);
                if (conversionType.equals("c2f")) {
                    celsius = doubleParser(args[1]);
                    fahrenheit = convertCelsiusToFahrenheit(celsius);
                    printToScreen(conversionType, celsius, fahrenheit);
                    break;
                } else if (conversionType.equals("f2c")){
                    fahrenheit = doubleParser(args[1]);
                    celsius = convertFahrenheitToCelsius(fahrenheit);
                    printToScreen(conversionType, celsius, fahrenheit);
                    break;
                }
            case 3:
                checkCorrectInputForConversionType(args[0]);
                System.out.println("Case 3: Error, please provide 'c2f' or 'f2c' and either 1 or 3 additional " +
                        "arguments");
                converterInstructions();
                break;
            case 4:
                checkCorrectInputForConversionType(args[0]);
                double start = doubleParser(args[1]);
                double finish = doubleParser(args[2]);
                double step = doubleParser(args[3]);
                printTableOfTemperatureConversions(conversionType, step, start, finish);
                break;
            default:
                System.out.println("Case default: Error, too many arguments provided.");
                converterInstructions();
                break;
        }
    }

    private static void checkCorrectInputForConversionType(String args){
        /*
         *   Check first argument is either c2f or f2c
         *   If not provide user instructions and terminate the program
         */
        conversionType = args;

        if ((!conversionType.equals("f2c")) && (!conversionType.equals("c2f"))) {
            System.out.println("ConversionType: Error, first argument must be 'c2f' or 'f2c'.");
            converterInstructions();
            System.exit(0);
        }
    }

    // args parse string to double
    private static double doubleParser(String args) {
        try {
            return Double.parseDouble(args);
        } catch (NumberFormatException nfe) {
            System.out.println("DoubleParser: Error, you must provide a number.");
            converterInstructions();
            System.exit(0); // exit program if number not found
            return 0;
        }
    }

    // Convert a single temperature from Fahrenheit to Celsius
    static double convertFahrenheitToCelsius(double fahrenheit) {
        return celsius = ((fahrenheit - 32) / 1.8);
    }

    // Convert a single temperature from Celsius to Fahrenheit
    static double convertCelsiusToFahrenheit(double celsius) {
        return fahrenheit = ((celsius * 1.8) + 32);
    }

    /*
    * Takes 3 arguments, step change, starting and finish point.
    * Prints to screen the temperature conversions incremented by the step value between the start and finish points
     */
    private static void printTableOfTemperatureConversions(String conversionType, double step, double start, double
            finish){

        switch (conversionType) {
            case "c2f":
                if (start <= finish) {
                    while (start <= finish) {
                        fahrenheit = convertCelsiusToFahrenheit(start);
                        printToScreen(conversionType, start, fahrenheit);
                        start = start + step;
                    }
                } else {
                    while (start >= finish) {
                        fahrenheit = convertCelsiusToFahrenheit(start);
                        printToScreen(conversionType, start, fahrenheit);
                        start = start - step;
                    }
                }
            case "f2c":
                if (start <= finish) {
                    while (start <= finish) {
                        celsius = convertFahrenheitToCelsius(start);
                        printToScreen(conversionType, celsius, start);
                        start = start + step;
                    }
                } else {
                    while (start >= finish) {
                        celsius = convertFahrenheitToCelsius(start);
                        printToScreen(conversionType, celsius, start);
                        start = start - step;
                }
            }
        }
    }

    // Print table of conversion to screen
    private static void printToScreen(String conversionType, double celsius, double fahrenheit) {

        switch (conversionType) {
            case "c2f":
                System.out.format("%.2f", celsius);
                System.out.print(" degrees Celsius equals ");
                System.out.format("%.2f", fahrenheit);
                System.out.print(" degrees Fahrenheit.");
                System.out.println();
            case "f2c":
                System.out.format("%.2f", fahrenheit);
                System.out.print(" degrees Fahrenheit equals ");
                System.out.format("%.2f", celsius);
                System.out.print(" degrees Celsius.");
                System.out.println();
        }
    }

    // Print converter instructions
    private static void converterInstructions(){
        System.out.println("\n*** Program Instructions ***");
        System.out.println("To convert Celsius to Fahrenheit enter 'c2f' as first argument.");
        System.out.println("To convert Fahrenheit to Celsius enter 'f2c' as first argument.");
        System.out.println("Follow this by 1 number to convert only 1 temperature.");
        System.out.println("Provide 3 numbers to convert a range of temperatures, in the following format;");
        System.out.println("Starting temperature, finish temperature and incremental amount.");
    }
    
}
